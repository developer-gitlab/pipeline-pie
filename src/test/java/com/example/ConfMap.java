package com.example;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ConfMap {
    private static Map<String, String> env = System.getenv();

    public static Map<String, String> getConfMap () {
        Map<String, String> confMap = new HashMap<>();
        confMap.putAll(env);
        return Collections.unmodifiableMap(confMap);
    }
}
