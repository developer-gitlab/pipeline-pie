package com.example;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Unit test for simple App.
 */
public class AppTest {
    private static final String STANDALONE_CHROME_HOST = ConfMap.getConfMap().get("STANDALONE_CHROME_HOST");

    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testMe() {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
        RemoteWebDriver driver = null;

        try {
            driver  = new RemoteWebDriver(new URL("http://" + STANDALONE_CHROME_HOST + ":4444/wd/hub"),desiredCapabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        driver.get("http://google.com");
        System.out.println("HELLO WORLD " + driver.getCurrentUrl());
        assertTrue(driver.getCurrentUrl().equals("https://www.google.com/?gws_rd=ssl"));

    }
}
